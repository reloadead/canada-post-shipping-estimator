'use strict';
const CanadaPostClient = require('canadapost-api');

const countries = require('../../../countries');

/**
*
* @param {string} from Postal code to send from
* @param {string} to Postal or zip code to send to
* @param {string} country Can be "Canada" or "US"
* @param {*} parcelCharacteristics Package details
*/
const getRate = async (to, country, parcelCharacteristics) => {
  try {
    const config = await strapi.query('config').findOne();
    const from = config.SenderZipCode;
    const cpClient = new CanadaPostClient(
      config.CanadaPostUserId,
      config.CanadaPostPassword,
      config.CanadaPostCustomerId,
      config.CanadaPostLang,
      config.CanadaPostTestEndpoint
    );

    let destination;
    switch (country) {
      case 'Canada':
        destination = { domestic: { postalCode: to } }
        break;
      case 'United States':
        destination = { unitedStates: { zipCode: to } }
        break;
      default:
        const countryCode = countries[country];
        destination = { international: { countryCode } }
        break;
    }

    const rates = await cpClient.getRates({
      originPostalCode: from,
      parcelCharacteristics,
      destination
    });

    if (rates && Array.isArray(rates) && rates.length > 0) {
      // Consider only rates that have Delivery Confirmation included
      const filteredRates = rates
        .filter((rate) =>
          rate.priceDetails
          && rate.priceDetails.options
          // options can be an array or a single object
          && (
            (Array.isArray(rate.priceDetails.options) && rate.priceDetails.options[0] && rate.priceDetails.options[0].optionCode === 'DC' && rate.priceDetails.options[0].optionPrice === '0')
            ||
            (rate.priceDetails.options.optionCode === 'DC' && rate.priceDetails.options.optionPrice === '0')
          )
        );
      // Find cheapest rate
      filteredRates
        .sort((rateA, rateB) => rateA.priceDetails.due - rateB.priceDetails.due);
      const rate = filteredRates[0];
      return {
        serviceName: rate.serviceName,
        price: rate.priceDetails.due,
        time: rate.serviceStandard.expectedTransitTime
      };
    }

  } catch (error) {
    console.error(error);
  }

  return null;
}

const round = (num) => Math.round((num + Number.EPSILON) * 100) / 100;

let updateRateIsRunning = false;
const updateRateQueue = [];

const updateRate = async (shippingRateId) => {
  if (updateRateIsRunning) {
    strapi.log.info('updateRate is already running, queueing...')
    updateRateQueue.push(shippingRateId);
  } else {
    updateRateIsRunning = true;
    const shippingRate = await strapi.query('shipping-rate').findOne(
      { id: shippingRateId },
      [
        'shipping_destination',
        'shipping_destination.id',
        'package',
        'package.Name',
        'package.Weight',
        'package.Len',
        'package.Width',
        'package.Height',
      ]
    );
    strapi.log.info(`Calculating rates for ${shippingRate.package.Name} to ${shippingRate.To}`);

    const shippingDestination = await strapi.query('shipping-destination').findOne(
      { id: shippingRate.shipping_destination.id },
      [
        'destinations',
        'destinations.ZipCode',
        'destinations.Country',
      ]
    )

    const parcelCharacteristics = {
      weight: shippingRate.package.Weight,
      dimensions: {
        length: shippingRate.package.Len,
        width: shippingRate.package.Width,
        height: shippingRate.package.Height
      }
    };

    const prices = [];
    const times = [];

    if (shippingRate) {
      for (const destination of shippingDestination.destinations) {
        const result = await getRate(destination.ZipCode, destination.Country, parcelCharacteristics)
          .then((rate) =>
            new Promise(resolve => setTimeout(() => resolve(rate), 1200))
          );
        if (result) {
          const { price, time } = result;
          prices.push(parseFloat(price));
          times.push(parseFloat(time));
        }
      }
      if (prices.length > 0) {
        const Price = round(prices.reduce((a, b) => a + b, 0) / prices.length)
        const DaysMin = round(Math.min(...times));
        const DaysMax = round(Math.max(...times));
        await strapi.query('shipping-rate').update(
          { id: shippingRate.id },
          { Price, DaysMin, DaysMax, Update: false }
        )
      }
    }
    updateRateIsRunning = false;
    strapi.log.info(`Done, checking the queue...`);
    if (updateRateQueue.length > 0) {
      updateRate(updateRateQueue.splice(0, 1));
    } else {
      strapi.log.info(`Nothing`);
    }
  }
}

module.exports = {
  updateRate
};
