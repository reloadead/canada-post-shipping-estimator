'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {

    afterCreate: async ({ id, Name }) => {
      // Create shipping rate for every destination type for new package
      const destinations = await strapi.query('shipping-destination').find();
      for (const destination of destinations) {
        await strapi.query('shipping-rate').create({
          To: destination.Name,
          shipping_destination: destination.id,
          PackageType: Name,
          package: id,
          Update: true
        });
      }
    },

    afterUpdate: async ({ id }) => {
      // Update related shipping rates
      const shippingRates = await strapi.query('shipping-rate').find({ 'package': id });
      for (const shippingRate of shippingRates) {
        await strapi.query('shipping-rate').update({ id: shippingRate.id }, { Update: true })
      }
    },

    afterDelete: async () => {
      // Delete orphaned shipping rates
      await strapi.query('shipping-rate').delete({ 'package.id_null': true });
    },
  }
};
