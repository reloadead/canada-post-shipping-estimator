'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {

    afterCreate: async ({ id, Name }) => {
      // Create shipping rate for every package type for new destination
      const packageTypes = await strapi.query('package').find();
      for (const packageType of packageTypes) {
        await strapi.query('shipping-rate').create({
          To: Name,
          shipping_destination: id,
          PackageType: packageType.Name,
          package: packageType.id,
          Update: true
        });
      }
    },

    afterDelete: async () => {
      // Delete orphaned shipping rates
      await strapi.query('shipping-rate').delete({ 'shipping_destination.id_null': true });
    },

  },
};
