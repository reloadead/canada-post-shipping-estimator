'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    afterUpdate: async ({ id, Update }) => {
      if (Update) {
        strapi.services['canada-post'].updateRate(id)
      }
    },
    afterCreate: async ({ id, Update }) => {
      if (Update) {
        strapi.services['canada-post'].updateRate(id)
      }
    }
  }
};
