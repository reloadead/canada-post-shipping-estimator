module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '7dc706488e11f91846e7b09b766ad325'),
    },
  },
  cron: {
    enabled: false
  }
});
